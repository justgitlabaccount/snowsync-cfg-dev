import requests
import logging
from environs import Env

# Настройка логгера для данного модуля
log = logging.getLogger(__name__)

# Загрузка переменных окружения
env = Env()
env.read_env()

class AuthManager:
    def __init__(self):
        self.api_key = env.str("API_KEY")
        self.api_url_token = "https://panel.netangels.ru/api/gateway/token/"
    
    def get_token(self):
        """
        Получает токен аутентификации, используя API-ключ.
        """
        try:
            # Выполнение POST-запроса для получения токена
            response = requests.post(self.api_url_token, data={"api_key": self.api_key})
            response.raise_for_status()  # Проверка на успешность запроса
            token = response.json()['token']  # Извлечение токена из ответа
            log.debug(f"Token retrieved successfully: {token}")
            return token
        except requests.exceptions.HTTPError as http_err:
            log.error(f"HTTP error occurred while retrieving token: {http_err} - {response.text}")
            raise
        except requests.exceptions.RequestException as err:
            log.error(f"Network-related error occurred while retrieving token: {err}")
            raise
        except KeyError:
            log.error(f"Token key was not found in response: {response.text}")
            raise
        except Exception as ex:
            log.error(f"An unexpected error occurred: {ex}")
            raise
