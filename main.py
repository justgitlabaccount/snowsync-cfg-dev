import logging
from config.auth import AuthManager
from dns_mng.dns_zone_mng import DNSZoneManager
from dns_mng.dns_records_mng import DNSRecordManager
from img_mng.dist_mng import DistManager
from environs import Env

# Настройка логгера
def setup_logging():
    """
    Настройка конфига логирования для приложения.

    Этот метод инициализирует конфигурацию логирования для приложения.
    Устанавливает уровень логирования DEBUG и настраивает формат лога.

    Returns:
        None
    """
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(filename)s:%(lineno)d #%(levelname)-8s [%(asctime)s] - %(name)s - %(message)s",
    )
    logger = logging.getLogger(__name__)
    logger.info("Logging is set up.")

def main():
    setup_logging()
    log = logging.getLogger(__name__)

    # Загрузка переменных окружения
    env = Env()
    env.read_env()

    # Аутентификация
    auth_manager = AuthManager()
    api_token = auth_manager.get_token()

    # Управление DNS
    dns_zone_manager = DNSZoneManager(api_token)
    dns_record_manager = DNSRecordManager(api_token)
    domain_name = 'syncjob.ru'
    dns_zone = dns_zone_manager.create_dns_zone(domain_name)
    dns_rec_a = dns_record_manager.create_dns_record(domain_name, 'A', '213.189.220.14')

    # Управление дистрибутивами
    dist_manager = DistManager(api_token)
    image_list = dist_manager.get_distr()
    dist_manager.save_images_to_file(image_list, 'files/images_list.txt')


if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        logging.error(f"An error occurred: {e}", exc_info=True)
