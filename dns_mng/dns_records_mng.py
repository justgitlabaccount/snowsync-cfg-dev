import requests
import logging

# Настройка логгера
log = logging.getLogger(__name__)

class DNSRecordManager:
    def __init__(self, api_token):
        self.api_token = api_token
        self.api_url_record = "https://api-ms.netangels.ru/api/v1/dns/records/"
        self.headers = {
            'Authorization': f'Bearer {self.api_token}',
            'Content-Type': 'application/json'
        }

    def create_dns_record(self, zone_name, record_type, ip_address):
        """
        Создает запись DNS типа 'A' для указанной зоны.
        """
        data = {
            'zone': zone_name,
            'type': record_type,
            'content': ip_address
        }
        
        try:
            # Выполнение POST-запроса для создания DNS записи
            response = requests.post(self.api_url_record, headers=self.headers, json=data)
            response.raise_for_status()  # Проверка на успешность запроса
            record_info = response.json()  # Получение информации о созданной записи
            log.debug(f"DNS Record created: {record_info}")
            return record_info
        except requests.exceptions.HTTPError as http_err:
            log.error(f"HTTP error occurred: {http_err} - {response.text}")
            raise
        except requests.exceptions.RequestException as err:
            log.error(f"Error communicating with NetAngels API: {err}")
            raise
        except Exception as ex:
            log.error(f"An unexpected error occurred: {ex}")
            raise
