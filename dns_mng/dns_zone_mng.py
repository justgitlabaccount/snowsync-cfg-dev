import requests
import logging

# Настройка логгера
log = logging.getLogger(__name__)

class DNSZoneManager:
    def __init__(self, api_token):
        self.api_token = api_token
        self.api_url_zone = "https://api-ms.netangels.ru/api/v1/dns/zones/"
        self.headers = {
            'Authorization': f'Bearer {self.api_token}',
            'Content-Type': 'application/json'
        }

    def create_dns_zone(self, zone_name):
        """
        Создает зону DNS с указанным именем.
        """
        data = {'name': zone_name}

        try:
            # Выполнение POST-запроса для создания DNS зоны
            response = requests.post(self.api_url_zone, headers=self.headers, json=data)
            response.raise_for_status()  # Проверка на успешность запроса
            zone_info = response.json()  # Получение информации о созданной зоне
            log.debug(f"DNS Zone created: {zone_info}")
            return zone_info
        except requests.exceptions.HTTPError as http_err:
            log.error(f"HTTP error occurred: {http_err} - {response.text}")
            raise
        except requests.exceptions.RequestException as err:
            log.error(f"Error communicating with NetAngels API: {err}")
            raise
        except Exception as ex:
            log.error(f"An unexpected error occurred: {ex}")
            raise
