import requests
import logging
import os

# Настройка логгера для данного модуля
log = logging.getLogger(__name__)

class DistManager:
    def __init__(self, api_token):
        self.api_token = api_token
        self.url_dist = "https://api-ms.netangels.ru/api/v1/images/distributions/"
        self.headers = {
            'Authorization': f'Bearer {self.api_token}',
            'Content-Type': 'application/json'
        }

    def get_distr(self):
        try:
            response = requests.get(self.url_dist, headers=self.headers)
            response.raise_for_status()
            distributions = response.json()  # Получение JSON ответа от API

            image_list = []

            # Проходим по всем группам, версиям и образам в полученном JSON
            for group in distributions['groups']:
                for version in group['versions']:
                    for image in version['images']:
                        image_list.append({
                            'id': image['id'],
                            'fullname': image['fullname'],
                            'cpu_count': image['requirements']['cpu_count'],
                            'memory_mb': image['requirements']['memory_mb']
                        })

            return image_list
        except requests.exceptions.HTTPError as http_err:
            log.error(f"HTTP error occurred: {http_err} - {response.text}")
            raise
        except requests.exceptions.RequestException as err:
            log.error(f"Error communicating with NetAngels API: {err}")
            raise
        except Exception as ex:
            log.error(f"An unexpected error occurred: {ex}")
            raise

    def save_images_to_file(self, images, filename):
        try:
            # Создание директории, если она не существует
            os.makedirs(os.path.dirname(filename), exist_ok=True)

            # Открытие файла для записи
            with open(filename, 'w') as file:
                # Запись каждого образа в файл в указанном формате
                for image in images:
                    line = f"ID: {image['id']}, Image: {image['fullname']}, Requirements: CPU {image['cpu_count']}, Memory {image['memory_mb']} MB\n"
                    file.write(line)
            log.info(f"Image list saved to {filename}")
        except Exception as ex:
            log.error(f"Failed to save images to file: {ex}")
            raise
